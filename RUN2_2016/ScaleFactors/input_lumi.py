#
# RUN 2 LUMINOSITY SUMMARY
#
# Luminosity value (muon json)
# 2045.732  pb
# Luminosity relative uncertainty
# 0.12 FIXME: unknown by now
#
# NOTE: lumi values must be ordered alphabetically
# NOTE: run evaluate_lumi.sh on it to have the calculated values for Lumi_mc^-1
#
# Lumi_mc^-1 VALUES:
#
# DY inclusive
6025.2/28696958.
# ttbar
815.96/186313000.
# tWt
35.6/988400.
# tWtbar
35.6/929046.
# W + jets
61526.7/18995071.
# WW inclusive
12.178/1996600.
# WZ
47.13/940600.
# ZZ
16.523/989312.
