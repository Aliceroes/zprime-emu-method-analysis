#
# RUN 2 LUMINOSITY SUMMARY
#
# Luminosity value (muon json)
# 6312.068  pb
# Luminosity relative uncertainty
# 0.05 FIXME: not sure
#
# NOTE: lumi values must be ordered alphabetically
# NOTE: run evaluate_lumi.sh on it to have the calculated values for Lumi_mc^-1
# NOTE: lines beginning with "#" are ignored
# NOTE: don't leave empty lines
#
# Lumi_mc^-1 = (sigma*kfactor)/ Nevents VALUES:
#
# DY inclusive
# old 6025.2/28696958.
6025.2/28647043.
#
# qcd1000to1400
9.4183/2967947.
# qcd120to170
471100./6848223.
# qcd1400to1800
0.84265/395725.
# qcd170to300
117276./6918748.
# qcd1800to2400
0.114943/393760.
# qcd2400to3200
0.00682981/398452.
# qcd300to470
7823./5968960.
# qcd3200
0.000165445/391108.
# qcd470to600
648.2/3977770.
# qcd600to800
186.9/3979884.
# qcd800to1000
32.293/3973224.
# qcd80to120
2762530./6953590.
#
# # dy120to200
# (19.32*1.006)/992000.
# # dy1400to2300
# (0.00139*1.006)/100000.
# # dy200to400
# (2.731*1.006)/100000.
# # dy2300to3500
# (0.00008948*1.006)/992000.
# # dy3500to4500
# (0.0000041*1.006)*100000.
# # dy400to800
# (0.241*1.006)/100000.
# # dy4500to6000
# (4.56e-7*1.006)/100000.
# # dy50to120
# (1975.*1.006)/2967200.
# # dy800to1400
# (0.01678*1.006)/100000.
# ttbar_lep
87.31/104649474.
# ttbar POWHEG
# 815.96/186313000.
# tW
35.6/998400.
#old 35.6/988400.
# Wantitop
35.6/989000.
#old 35.6/929046.
# Wjets
61526.7/18995071.
#
# WW1200to2500
0.0035/200000.
# WW200to600
1.385/200000.
# WW2500
0.00005/38969.
# WW600to1200
0.0566/200000.
#
# WW inclusive
12.178/1996600.
#
# WZ
47.13/1000000.
#old:47.13/940600.
# ZZ
16.523/989312.
#
#
