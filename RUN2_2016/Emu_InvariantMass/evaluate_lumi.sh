#!/bin/bash
# To make the script executable: chmod u+x evaluate_lumi.sh
# To run the script: ./evaluate_lumi.sh
#
# Takes values from an input file containing mathematical formulas
# and evaluates the result, saving it in a .dat file
# Precisely: EVALUATES LUMI_MC^-1 VALUES FOR VARIOUS MC BACKGROUNDS
# AND SAVES THEM IN lumi_mc_file.dat
#
echo
echo "EVALUATING:"
# NOTE: # characters are interpreted by python as comments
# But this code reads every word separated by space, so doesn't interpret
# lines beginning with # as comment lines
#    cat output_lumi.dat
#    for line in `cat output_lumi.dat`; do python -c "print $line"; done > lumi_mc_file.dat
# This code indeed does:
cat input_lumi.py
cat input_lumi.py | while read -r line; do python -c "print  $line"; done > lumi_mc_file.dat
#echo "Result:"
#cat result.dat
echo
