// NOTE: Required ROOT version 6.04
R__LOAD_LIBRARY(../Libraries/Build/libAlicesTools.dylib)


void checkcutzconfronto()
{



  gROOT -> Reset();



  // -- OPEN FILE FOR MC FOR SIGNAL (RSG KK 3TeV) AND GET "h_tau32" HISTOGRAM

  const char *filepath = "files/mieioutputN/cutChecksRSG3TeV.root";
  const char *histoname = "h_mttbar";
  histosfromtfile gethistograms;
  TH1F *data1 = gethistograms.histo(filepath, histoname);



  // -- OPEN ROOTFILES FOR DY and get histograms NOISO
  const char *dirname = "/afs/cern.ch/work/a/aalfonsi/public/mcTRGOFF/";
  const char *ext = ".root";
  // const char *histoarrayname = "NoIso/LeptonPhiTrackerFailedCut";
  // const char *histoarrayname = "NoIso/LeptonPtTrackerFailedCut";
  // const char *histoarrayname = "NoIso/LeptonEtaTrackerFailedCut";
  // const char *histoarrayname = "NoIso/NVerticesFailedCut";
  const char *histoarrayname = "NoIso/IsoNTracksFailedCut";
  // const char *histoarrayname = "NoIso/IsoNJetsFailedCut";

  // const char *histoarrayname = "NoIso/RelIsoSumPt";

  const char *strip = "";
  TH1F **histoarray = gethistograms.histoarray(dirname, ext, histoarrayname, strip);
  Int_t n = gethistograms.getn();   // number of histograms in the array


  // -- OPEN ROOTFILES FOR DY and get histograms NONO
  // const char *histoarraynameNoNo = "NoNo/LeptonPhiTracker";
  // const char *histoarraynameNoNo = "NoNo/LeptonPtTracker";
  // const char *histoarraynameNoNo = "NoIso/LeptonEtaTracker";
  // const char *histoarraynameNoNo = "NoNo/NVertices";
  const char *histoarraynameNoNo = "NoNo/IsoNTracks";
  // const char *histoarraynameNoNo = "NoNo/IsoNJets";

  // const char *histoarraynameNoNo = "NoIso/RelIsoSumPt";

  TH1F **histoarrayNoNo = gethistograms.histoarray(dirname, ext, histoarraynameNoNo, strip);

// for (Int_t i=0; i<9; i++)
// {
//   histoarray[i]->Sumw2();
//   histoarrayNoNo[i]->Sumw2();
// }



  // -- NORMALIZE HISTOGRAMS

  // w_norm = Lumi_data / Lumi_mc = Lumi_data / (N_mc / xsection_mc)
  Float_t Lumi_data = 1.;                  // Luminosity value

  // Retrieve mc luminosities
  lumitools lumitools;
  Float_t Lumi_mcSignal = 99755./12.99;
  Float_t Lumi_mcBg[n]; // Lumi
  Float_t w_norm[n];
  Lumi_mcBg[0] = 99200. / (19.32*1.006);    // 120to200
  Lumi_mcBg[1] = 100000. / (0.00139*1.006)    ; // 1400to2300
  Lumi_mcBg[2] = 100000. / (2.731*1.006)  ;   // 200to400
  Lumi_mcBg[3] = 99200. / (0.00008948*1.006)  ;   // 2300to3500
  Lumi_mcBg[4] = 100000. / (0.0000041*1.006)   ;   // 3500to4500
  Lumi_mcBg[5] = 100000. / (0.241*1.006)   ;  // 400to800
  Lumi_mcBg[6] = 100000. / (4.56E-7*1.006)   ;  // 4500to6000
  Lumi_mcBg[7] = 2967200. / (1975*1.006)  ; // 50to120
  Lumi_mcBg[8] = 100000. / (0.01678*1.006)   ;  // 800to1400

  // Clone histograms before scale to save errors and reassign prescale errors
  // TH1F* histoarrayclone[n];
  // TH1F* histoarrayNoNoclone[n];
  // for (Int_t i=0; i<n; i++)
  // {
  //   histoarrayclone[i] = (TH1F*)histoarray[i]->Clone(histoarray[i]->GetName());
  //   histoarrayNoNoclone[i] = (TH1F*)histoarrayNoNo[i]->Clone(histoarrayNoNo[i]->GetName());
  // }


  Int_t er = histoarray[0]->GetNbinsX()-30;
  histoarray[0]->Sumw2();
  cout << "\nBIN: " << er << endl;
  cout << "Lumi0: " << Lumi_mcBg[0] << endl;
  cout << "errors:\n";
  cout << histoarray[0]->GetBinError(er) << endl;
  cout << histoarray[0]->GetBinContent(er) << endl;
  // cout << histoarrayclone[0]->GetBinError(100) << endl;
  // cout << "ok" <<;
  // for (Int_t k=0; k<histoarray[0]->GetNbinsX(); k++)
  // {
  //   cout << histoarray[0]->GetBinContent(k) << endl;
  //   cout << histoarray[0]->GetBinError(k) << endl;
  //   cout << histoarrayclone[0]->GetBinError(k) << endl << endl;
  //   histoarray[i]->SetBinError(k,histoarrayclone[i]->GetBinError(k));
  //   histoarrayNoNo[i]->SetBinError(k,histoarrayNoNoclone[i]->GetBinError(k));
  // }

  // Scale mc histograms and reassign errors prescale
  cout << "errori bin" << er << "ogni itogramma:\n";
  Float_t w = Lumi_data/Lumi_mcSignal;
  for (Int_t i=0; i<n; i++)
  {
    histoarray[i] -> Scale(1./Lumi_mcBg[i]);
    histoarrayNoNo[i] -> Scale(1./Lumi_mcBg[i]);
    cout << i << ": " << histoarray[i]->GetBinError(er) << endl;
    for (Int_t k=0; k<histoarray[0]->GetNbinsX(); k++)
    {
      // cout << i << ": " << histoarray[i]->GetBinError(er) << endl;
      // cout << histoarrayclone[0]->GetBinError(k) << endl << endl;
      // histoarray[i]->SetBinError(k,histoarrayNoNoclone[i]->GetBinError(k));
      // histoarrayNoNo[i]->SetBinError(k,histoarrayNoNoclone[i]->GetBinError(k));
    }
  }


  cout << "errors dopo scale:\n";
  cout << histoarray[0]->GetBinError(er) << endl;
  cout << histoarray[0]->GetBinContent(er) << endl;
  // -- SUM BG HISTOGRAMS
  // TList *mergedDY = new TList;
  // for (Int_t i=0; i<n; i++)
  // {
  //   TString histoname = histoarray[i]->GetName();
  //   mergedDY -> Add(histoarray[i]);
  //   cout << "\nAdded to bg: ";
  //   cout << histoname;
  // }
  // TH1F *DY_merge = new TH1F;
  // DY_merge -> SetDirectory(gROOT);
  // DY_merge -> Merge(mergedDY);
  // DY_merge -> SetName("mergedDY");


// ADD DY NOISO
  histoarray[0] -> Add(histoarray[1]);
  histoarray[0] -> Add(histoarray[2]);
  histoarray[0] -> Add(histoarray[3]);
  histoarray[0] -> Add(histoarray[4]);
  histoarray[0] -> Add(histoarray[5]);
  histoarray[0] -> Add(histoarray[6]);
  histoarray[0] -> Add(histoarray[7]);
  histoarray[0] -> Add(histoarray[8]);


// ADD DY NONO
  histoarrayNoNo[0] -> Add(histoarrayNoNo[1]);
  histoarrayNoNo[0] -> Add(histoarrayNoNo[2]);
  histoarrayNoNo[0] -> Add(histoarrayNoNo[3]);
  histoarrayNoNo[0] -> Add(histoarrayNoNo[4]);
  histoarrayNoNo[0] -> Add(histoarrayNoNo[5]);
  histoarrayNoNo[0] -> Add(histoarrayNoNo[6]);
  histoarrayNoNo[0] -> Add(histoarrayNoNo[7]);
  histoarrayNoNo[0] -> Add(histoarrayNoNo[8]);

// DIVIDE
cout << "n bins\n";
cout << histoarray[0]->GetXaxis()->GetXmin() << endl << histoarray[0]->GetXaxis()->GetXmax() << endl;
cout << histoarray[0]->GetNbinsX() << endl;
cout << histoarrayNoNo[0]->GetNbinsX() << endl;
  // TH1F* division = new TH1F("division", "", 100, -5., 5.);
  // division -> Divide(histoarray[0], histoarrayNoNo[0]);



  cout << "errors dopo add:\n";
  cout << histoarray[0]->GetBinError(er) << endl;
  cout << histoarray[0]->GetBinContent(er) << endl;

  // APPEARANCE


  // styles mystyles;
  // TStyle *st1 = mystyles.AlicesStyle1();
  // gROOT -> SetStyle("st1");
  // gROOT -> ForceStyle();

  // Histograms appearance
  // DY_merge -> SetLineColor(36);
  // DY_merge -> SetFillColor(33);
  histoarray[0] -> SetLineColor(kAzure);
  histoarray[0] -> SetFillColor(33);
  histoarray[0] -> SetMarkerStyle(8);
  histoarray[0] -> SetMarkerColor(kAzure);
  histoarray[0] -> SetMarkerSize(0.5);

  // division -> SetLineColor(kViolet-3);
  // division -> SetFillColor(33);
  // division -> SetMarkerStyle(8);
  // division -> SetMarkerColor(kBlack);
  // division -> SetMarkerSize(0.5);

  histoarrayNoNo[0] -> SetLineColor(kRed);
  histoarrayNoNo[0] -> SetFillColor(33);
  histoarrayNoNo[0] -> SetMarkerStyle(8);
  histoarrayNoNo[0] -> SetMarkerColor(kRed);
  histoarrayNoNo[0] -> SetMarkerSize(0.5);
  // gStyle->SetPalette(1,0);                        // pretty and useful palette


  TString plotTitle = histoarrayname;
  plotTitle.ReplaceAll("FailedCut","");
  plotTitle.ReplaceAll("NoIso/","");
  plotTitle.ReplaceAll("Tracker","");
  plotTitle.ReplaceAll("Lepton","");
  plotTitle.ReplaceAll("Pt"," p_{T} ");
  plotTitle.ReplaceAll("Eta"," #eta ");
  plotTitle.ReplaceAll("Phi"," #phi ");
  plotTitle.ReplaceAll("RelIsoSum p_{T}","Rel Iso Sum p_{T} ");
  plotTitle.ReplaceAll("NVertices"," #Vertices ");
  plotTitle.ReplaceAll("IsoNJets"," Iso. (#Delta R < 0.3) #Jets ");
  plotTitle.ReplaceAll("IsoNTracks"," Iso. (#Delta R < 0.3) #Tracks ");

  // TLatex *title = new TLatex(-2.9,0.118,plotTitle);
  histoarray[0] -> SetTitle(plotTitle);
  histoarray[0]->SetName("mergedDY");
  histoarrayNoNo[0] -> SetTitle(plotTitle);
  histoarrayNoNo[0]->SetName("mergedDY");

  // division -> SetTitle(plotTitle);
  // division->SetName("IsoFailed/NoIso");



  // -- DRAW HISTOGRAMS


 TCanvas* c1 = new TCanvas();
 // histoarrayNoNo[0] -> DrawNormalized("same");
 // histoarray[0]->DrawNormalized();
 histoarray[0] -> GetXaxis() -> SetTitleOffset(1.1);
 histoarray[0] -> GetYaxis() -> SetTitleOffset(1.15);
 histoarray[0] -> GetYaxis() -> SetTitle("Events");
 histoarray[0] -> GetXaxis() -> SetTitle(plotTitle);
 // division -> GetXaxis() -> SetTitleOffset(1.1);
 // division -> GetYaxis() -> SetTitleOffset(1.3);
 // division -> GetYaxis() -> SetTitle("Events");
 // division -> GetXaxis() -> SetTitle(plotTitle);


// Ranges
if (plotTitle.Contains("eta"))
{
  cout << "eta";
  histoarray[0]->GetXaxis()->SetRangeUser(-2.5,2.5);
  histoarray[0]->GetYaxis()->SetRangeUser(1e-4,3);
  c1-> SetLogy();
  histoarray[0] -> Draw("same");
  histoarrayNoNo[0] -> Draw("same");
  // division -> Draw("same");

}


if (plotTitle.Contains("Tracks"))
{
  cout << "tracks";
  // histoarray[0]->GetXaxis()->SetRangeUser(-2.5,2.5);
  histoarray[0]->GetYaxis()->SetRangeUser(1e-4,1e2);
  c1-> SetLogy();
  histoarray[0] -> Draw("same");
  histoarrayNoNo[0] -> Draw("same");
}


if (plotTitle.Contains("Vertices"))
{
  cout << "vertices";
  // histoarray[0]->GetXaxis()->SetRangeUser(-2.5,2.5);
  histoarray[0]->GetYaxis()->SetRangeUser(1e-9,3e2);
  c1-> SetLogy();
  histoarray[0] -> Draw("same");
  histoarrayNoNo[0] -> Draw("same");
}


if (plotTitle.Contains("phi"))
{
  cout << "phi";
  histoarray[0] -> GetXaxis() -> SetTitleOffset(1);
  histoarray[0] -> Rebin(2);
  histoarrayNoNo[0] -> Rebin(2);
  histoarray[0]->GetXaxis()->SetRangeUser(-2.5,2.5);
  // gStyle->SetStripDecimals(kTRUE);
  histoarray[0]->GetYaxis()->SetRangeUser(1e-3,1);
  c1-> SetLogy();
  histoarray[0] -> Draw("same");
  histoarrayNoNo[0] -> Draw("same");
}


if (plotTitle.Contains("Sum p_{T}"))
{
  cout << "sumpt";
  // histoarray[0] -> Rebin(10);
  // histoarrayNoNo[0] -> Rebin(10);
  histoarray[0]->GetXaxis()->SetRangeUser(0.,1.);
  // gStyle->SetStripDecimals(kFALSE);
  // histoarray[0]->GetYaxis()->SetRangeUser(1e-10,1e1);
  c1-> SetLogy();
  histoarray[0] -> Draw("same");
  // histoarrayNoNo[0] -> Draw("same");
}
else if (plotTitle.Contains("p_{T}"))
{
  cout << "pt";
  histoarray[0] -> Rebin(10);
  histoarrayNoNo[0] -> Rebin(10);
  histoarray[0]->GetXaxis()->SetRangeUser(0.,1500.);
  // gStyle->SetStripDecimals(kFALSE);
  histoarray[0]->GetYaxis()->SetRangeUser(1e-10,1e1);
  c1-> SetLogy();
  histoarray[0] -> Draw();
  histoarrayNoNo[0] -> Draw("same");
}



if (plotTitle.Contains("Jets"))
{
  cout << "jets";
  histoarray[0]->GetXaxis()->SetRangeUser(0,3);
  histoarray[0]->GetYaxis()->SetRangeUser(0.,0.1903);
  // c1-> SetLogy();
  // histoarray[0] -> Draw("axis");
  histoarray[0]->DrawNormalized("same");
  histoarrayNoNo[0]->DrawNormalized("same");
}
std::cout << "\nSOMMA HISTO: " << histoarray[0]->GetEntries();
std::cout << "\nSOMMA HISTONONO: " << histoarrayNoNo[0]->GetEntries();


gStyle -> SetOptStat(0);
gStyle->SetStatX(0.885);		//Stat box x position (top right hand corner)
gStyle->SetStatY(0.875); 		//Stat box y position
// gStyle->SetStatW(0.09);	 		//Stat box width as fraction of pad size
// gStyle->SetStatH(0.09);	 		//Size of each line in stat box
// gStyle->SetStatColor(0);		//Stat box fill color
// gStyle->SetStatTextColor(1);		//Stat box text color
// gStyle->SetStatStyle(1001);		//Stat box fill style!
gStyle->SetLineColor(797);		//Stat box fill style!
// //StatFont = 10*fontid + 2 (12 is normal, 22 bold 32 italic)
// gStyle->SetStatFont(62);  		//Stat box fond
gStyle->SetStatBorderSize(0);		//Stat box border thickness
gStyle->SetStatColor(18);
gPad->Update();
// gStyle->SetStatX(0.995);		//Stat box x position (top right hand corner)
// gStyle->SetStatY(0.995); 		//Stat box y position
// gStyle->SetStatW(0.09);	 		//Stat box width as fraction of pad size
// gStyle->SetStatH(0.09);	 		//Size of each line in stat box
// gStyle->SetStatColor(0);		//Stat box fill color
// gStyle->SetStatTextColor(1);		//Stat box text color
// gStyle->SetStatStyle(1001);		//Stat box fill style!
// //StatFont = 10*fontid + 2 (12 is normal, 22 bold 32 italic)
// gStyle->SetStatFont(62);  		//Stat box fond
// gStyle->SetStatBorderSize(2);		//Stat box border thickness
// gStyle->SetStatStyle(0);
// gStyle->SetTitleStyle(0);
// gStyle->SetStatFormat("6.4g");
// TPaveStats *s = (TPaveStats*) gPad->GetPrimitive("stats");
// // s->SetTextSize(0.1);
// s->SetTextColor(1);
// s->SetFillColor(18);
// s->SetBorderSize(0);
// s->SetLineColor(3);
// gStyle->SetStatStyle(0);
// gStyle->SetTitleStyle(0);
//gStyle->SetStatFormat("6.4g");
// s->SetStatStyle(0)
// s -> Draw();
gPad->Update();



// Legend
TString dynoiso = "DY NoIso ";
TString dynono = "DY NoNo";
TString zero = "";
TString entries = " entries";
TLegend *leg = new TLegend(0.72,0.76,0.93,0.93);
leg -> SetTextFont(43);
leg -> SetTextSizePixels(16);
leg -> SetBorderSize(0);
leg -> SetFillColorAlpha(18, 1);
leg -> AddEntry(histoarray[0], "DY IsoFailed",  "lp");
leg -> AddEntry(histoarray[0], Form ("%3.0f", histoarray[0]->GetEntries())+ entries,  "");
leg -> AddEntry(histoarrayNoNo[0], "DY Isolated",  "lp");
leg -> AddEntry(histoarrayNoNo[0], Form("%3.0f", histoarrayNoNo[0]->GetEntries())+ entries,  "");
// leg -> AddEntry((TObject*)0, zero + '('+Form("%3.0f", histoarrayNoNo[0]->GetEntries())+ entries,  "lp");
// leg -> AddEntry("dyInclusiveTauTau", "DY #rightarrow #tau#tau",  "fl");
leg -> Draw();


}
