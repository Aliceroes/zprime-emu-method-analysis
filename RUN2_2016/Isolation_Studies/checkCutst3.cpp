// NOTE: Required ROOT version 6.04
R__LOAD_LIBRARY(../Libraries/Build/libAlicesTools.dylib)


void checkCuts3()
{



  gROOT -> Reset();



  // -- OPEN FILE FOR MC FOR SIGNAL (RSG KK 3TeV) AND GET "h_tau32" HISTOGRAM

  const char *filepath = "files/mieioutputN/cutChecksRSG3TeV.root";
  const char *histoname = "h_bdisc";
  histosfromtfile gethistograms;
  TH1F *data1 = gethistograms.histo(filepath, histoname);



  // -- OPEN ROOTFILES FOR MC BG AND GET "h_tau32" HISTOGRAMS

  const char *dirname = "files//mieioutputN/bg/";
  const char *ext = ".root";
  const char *histoarrayname = "h_bdisc";
  const char *strip = "cutChecks_";
  TH1F **histoarray = gethistograms.histoarray(dirname, ext, histoarrayname, strip);
  Int_t n = gethistograms.getn();   // number of histograms in the array




  // -- NORMALIZE HISTOGRAMS

  // w_norm = Lumi_data / Lumi_mc = Lumi_data / (N_mc / xsection_mc)
  Float_t Lumi_data = 12.90;                  // Luminosity value

  // Retrieve mc luminosities
  lumitools lumitools;
  Float_t Lumi_mcSignal = 99755./12.99;
  Float_t Lumi_mcBg[n];
  Float_t w_norm[n];
  Lumi_mcBg[0] = 92925926. / 831.76;     // ttjets
  Lumi_mcBg[1] = 246737. / 1.61    ; // Wjets1200to2500
  Lumi_mcBg[2] = 253561. / 0.0389  ;   // Wjets2500
  Lumi_mcBg[3] = 3722395. / 14.58  ;   // Wjets600to800
  Lumi_mcBg[4] = 1540477. / 6.66   ;  // Wjets800to1200



  // Scale mc histograms
  Float_t w = Lumi_data/Lumi_mcSignal;
  data1 -> Scale(w);
  for (Int_t i=0; i<n; i++)
  {
    histoarray[i] -> Scale(Lumi_data/Lumi_mcBg[i]);
  }

//histoarray[4] -> Draw();


  // -- SUM BG HISTOGRAMS

//histoarray[0] -> Draw();
  histoarray[0] -> Add(histoarray[1]);
  histoarray[0] -> Add(histoarray[2]);
  histoarray[0] -> Add(histoarray[3]);
  histoarray[0] -> Add(histoarray[4]);
// histoarray[0]-> Draw();
// histoarray[1]-> Draw("same");

  // TList *mcBg = new TList;
  // for (Int_t i=0; i<n; i++)
  // {
  //   TString histoname = histoarray[i]->GetName();
  //   mcBg -> Add(histoarray[i]);
  //   cout << "\nAdded to bg: ";
  //   cout << histoname;
  // }
  // TH1F *mcBg_merge = new TH1F;
  // mcBg_merge -> SetDirectory(gROOT);
  // mcBg_merge -> Merge(mcBg);
  // mcBg_merge -> SetName("mergedBg");


  //
  // // DIVIDE DATA / MC
  //
  // TH1F *datamc_division = new TH1F("datamc_division", "", 1000, 0, 20000);
  // datamc_division -> SetDirectory(gROOT);
  // TH1F *stacksum = (TH1F*)hs -> GetStack() -> Last();
  // stacksum -> SetDirectory(gROOT);
  // datamc_division -> Divide(data1, stacksum);



  // APPEARANCE

  // TStyle
  styles mystyles;
  TStyle *st1 = mystyles.AlicesStyle1();
  gROOT -> SetStyle("st1");
  gROOT -> ForceStyle();

  // Histograms appearance
  data1 -> SetLineColor(2);
  histoarray[0] -> SetLineColor(3);



  // -- DRAW HISTOGRAMS
//TH2D* rangesitter = new TH2D("rangesitter", "rangesetter", 100, 100, 2, 100, 1e-2, 1e4);
//rangesitter->Draw();
histoarray[0] -> Draw();

  data1 -> Draw("same");






  // // To easily set the x, y range for all the histograms I am creating a new
  // // empty TH2D for each pad and then printing my histograms on top of it
  // TH2D* rangesitter = new TH2D("rangesitter", "rangesetter", 100, 100, 2000, 100, 1e-2, 1e4);
  // TH2D* rangesitterb = new TH2D("rangesitterb", "rangesetterb", 100, 100, 2000, 100, 0., 2.);
  // rangesitter -> GetXaxis() -> SetTickLength(0);
  // rangesitter -> GetXaxis() -> SetLabelOffset(999);
  // rangesitter -> GetYaxis() -> SetTitle("Events / 20 GeV");
  // rangesitterb -> GetXaxis() -> SetTitle("mass_{e#mu} (GeV)");
  // rangesitterb -> GetYaxis() -> SetTitle("Data / MC ");
  //
  // // Canvas & pads
  // TwoPadsTCanvas *c1 = new TwoPadsTCanvas("MC and data histograms", "MC and data histograms", 0, 0, 1000, 700);
  // TPad* pad1 = c1->pad1;
  // TPad* pad2 = c1->pad2;
  // TPad* erase = c1->erase;
  //
  // // Top pad
  // pad1 -> cd();
  // rangesitter -> Draw();
  // hs -> Draw("Same,HIST");
  // data1 -> Draw("Same");
  // rangesitter -> Draw("sameaxis");
  //
  // // Legend
  // TLegend *leg = new TLegend(0.76,0.5,0.92,0.83);
  // leg -> SetTextFont(43);
  // leg -> SetTextSizePixels(14);
  // leg -> SetBorderSize(0);
  // leg -> SetFillColorAlpha(0, 0.);
  // leg -> AddEntry(data1, "Data 2016",  "fl");
  // leg -> AddEntry("mergedDi", "Dibosons",  "fl");
  // leg -> AddEntry("mergedtW", "tWt + tW#bar{t}",  "fl");
  // leg -> AddEntry("ttbar_lep", "t#bar{t}",  "fl");
  // leg -> AddEntry("mergedjets", "Jets",  "fl");
  // leg -> Draw();
  //
  // // Bottom pad
  // pad2 -> cd();
  // rangesitterb -> Draw();
  // TLine *lined = new TLine(100,1,2000,1);
  // lined -> SetLineColor(kGray+1);
  // lined -> Draw();
  // datamc_division -> Draw("Same");



  // signal significance S/sqrtB
  SB = (data1->GetSumOfWeights() )/(histoarray[0]->GetSumOfWeights());
  cout << endl << SB;




}
