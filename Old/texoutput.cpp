{


  // RETRIEVE STORED CONTENT
  // Open ROOT file and get storage TGraphs
  TFile *texout = TFile::Open("texoutput.root", "READ");
  texout -> ls();
  TGraph *g;
  TGraph *T_SFtot;
  TGraph *T_N_emu_obs;
  TGraph *T_N_mumu_data;
  TGraph *T_N_mumu_sim;
  texout -> GetObject("SFtot", T_SFtot);
  texout -> GetObject("N_emu_obs", T_N_emu_obs);
  texout -> GetObject("N_mumu_sim", T_N_mumu_sim);
  texout -> GetObject("N_mumu_data", T_N_mumu_data);
  delete texout;
  // Retrieve arrays
  Double_t *SFtot = T_SFtot -> GetY();
  Double_t *N_emu_obs = T_N_emu_obs -> GetY();
  Double_t *N_mumu_data = T_N_mumu_data -> GetY();
  Double_t *N_mumu_sim = T_N_mumu_sim -> GetY();


  // WRITE .TEX TABLE
  // Then just copy and paste this table into the .tex file to use it
  FILE *out_data;
  out_data = fopen("table.tex", "w");

  fprintf (out_data,
  "\\begin{center}\n"
  "\\begin{tabular}{| c | c | c | c | c |}\n"
  "\\hline\n"
  "Mass range "
  "& \\begin{tabular}[c]{@{}c@{}}$N (e^{\\pm} \\mu^{\\pm})$\\\\ observed\\\\ \\end{tabular}\n"
  "& \\begin{tabular}[c]{@{}c@{}}$\\mu\\mu / e\\mu$\\\\scale factor\\end{tabular}\n"
  "& \\begin{tabular}[c]{@{}c@{}}$N (\\mu^{+} \\mu^{-})$\\\\data prediction\\end{tabular}\n"
  "& \\begin{tabular}[c]{@{}c@{}}$N (\\mu^{+} \\mu^{-})$\\\\sim. prediction\\end{tabular}\n"
  "\\\\ \\hline\n"
  "120$-$200 GeV & %.0f & %.3f & %.0f & %.0f\\\\ \\hline\n"
  "200$-$400 GeV & %.0f & %.3f & %.0f & %.0f\\\\ \\hline\n"
  "$>$ 400 GeV & %.0f & %.3f & %.0f & %.0f\\\\ \\hline\n"
  "\\end{tabular}\n"
  "\\end{center}\n",
  N_emu_obs[0], SFtot[0], N_mumu_data[0], N_mumu_sim[0],  N_emu_obs[1], SFtot[1], N_mumu_data[1], N_mumu_sim[1],  N_emu_obs[2], SFtot[2], N_mumu_data[2], N_mumu_sim[2]);

  fclose (out_data);


}
