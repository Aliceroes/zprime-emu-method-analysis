{
  /**
       Calculates Scale Factors SF with the e-mu method and draws different
       histograms ("Reconstructed events / bin", "Scale Factors", "Fractions")
       to show their relevance
  */

  // NOTE: using files hosted in my afs cell: need to login
  // kinit aalfonsi@CERN.CH    <-- kerberos login
  // [insert password]
  // aklog    <-- afs cell login


  gROOT -> Reset();

  // -- OPEN DATA ROOTFILE AND TAKE "DileptonMass" HISTOGRAM
  Char_t datafilename[500] = "/afs/cern.ch/work/a/aalfonsi/Files_analisi/Data_Nov2015/Run2015MuonsOnly/ana_datamc_data.root";
  TFile *f = new TFile(datafilename);
  if (!f || f -> IsZombie())
    return;
  std::cout << ".................................................\n";
  std::cout << "Loaded data file: " << std::endl;
  std::cout << "* - " << datafilename << std::endl;
  TH1F * data1 = (TH1F*)f -> Get("EmuVetoMuonsElectronsOppSignHistos/DileptonMass");
  data1 -> SetDirectory(0);
  f -> Close();


  // -- OPEN MC ROOTFILES AND GET "DileptonMass" HISTOGRAMS
  // List rootfiles
  const char* ext = ".root";
  const char* dirbase = "/afs/cern.ch/work/a/aalfonsi/Files_analisi/mcStartupPUrw/";
  const char* filename;
  const char* rootfilename[100];
  Int_t n = 0;
  char* dir = gSystem -> ExpandPathName(dirbase);
  void* direxp = gSystem -> OpenDirectory(dir);
  while((filename = (char*)gSystem -> GetDirEntry(direxp)))
  {
    TString str;
    str = filename;
    if((str.EndsWith(ext)) && (!str.Contains("dy")))
      rootfilename[n++] = gSystem -> ConcatFileName(dir, filename);
  }
  // Book TFiles / histograms
  TFile **filearray = new TFile*[n];
  TH1F **histoarrayemu = new TH1F*[n];
  TH1F **histoarraymumu = new TH1F*[n];
  std::cout << "Loaded mc files: " << std::endl;
  for (Int_t i = 0; i < n; i++)
  {
    Char_t currentfile[500];
    sprintf(currentfile,"%s",rootfilename[i]);
    filearray[i] = new TFile(currentfile);
    if (!filearray[i] || filearray[i] -> IsZombie())
      return;
    TH1::AddDirectory(kFALSE);
    histoarrayemu[i] = (TH1F*)filearray[i] -> Get("EmuVetoMuonsElectronsOppSignHistos/DileptonMass");
    histoarraymumu[i] = (TH1F*)filearray[i] -> Get("Our2012MuonsPlusMuonsMinusHistos/DileptonMass");
    std::cout << i << " - " << rootfilename[i] << std::endl;
    filearray[i] -> Close();
  }
  gSystem -> FreeDirectory(direxp);


  // -- NORMALIZE HISTOGRAMS
  // Calculate weight factors
  // w_norm = Lumi_data / Lumi_mc = Lumi_data / (N_mc / xsection_mc)
  Float_t Lumi_data = 2670.745 ;               // Right luminosity value;
  Float_t Lumi_mc_inv[16];                     // Lumi_mc^(-1)
  Float_t w_norm[7];
  Lumi_mc_inv[12] = 61500./24230960.;          // W + jets
  Lumi_mc_inv[15] = 15.4/998848.;              // ZZ
  Lumi_mc_inv[14] = 66.1/996920;               // WZ
  Lumi_mc_inv[13] = 118.7/995192.;             // WW
  Lumi_mc_inv[10] = 35.85/1000000.;            // tWtbar
  Lumi_mc_inv[11] = 35.85/998400.;             // tWt
  Lumi_mc_inv[9] = 815.96/19699896.;           // ttbar
  //6025.2/13344402.;                          // DY inclusive
  Lumi_mc_inv[7] = (1975.*1.006)/2901238.;     // DY 50 - 120
  Lumi_mc_inv[0] = (19.32*1.006)/100000.;      // DY 120 - 200
  Lumi_mc_inv[2] = (2.731*1.006)/100000.;      // DY 200 - 400
  Lumi_mc_inv[5] = (0.241*1.006)/100000.;      // DY 400 - 800
  Lumi_mc_inv[8] = (0.01678*1.006)/100000.;    // DY 800 - 1400
  Lumi_mc_inv[1] = (0.00139*1.006)/99600.;     // DY 1400 - 2300
  Lumi_mc_inv[3] = (0.00008948*1.006)/100000.; // DY 2300 - 3500
  Lumi_mc_inv[4] = (0.0000041*1.006)/100000.;  // DY 3500 - 4500
  Lumi_mc_inv[6] = (4.56E-7*1.006)/100000.;    // DY 4500 - 6000
  for (Int_t i=0; i<n; i++)
  {
    w_norm[i] = Lumi_data * Lumi_mc_inv[i+9];
    histoarrayemu[i] -> Scale(w_norm[i]);
    histoarraymumu[i] -> Scale(w_norm[i]);
  }


  // REBIN HISTOGRAMS
  const Int_t numbins = 3;
  Double_t newBins[] = {120., 200., 400., 600.};
  // NOTE: Rebin needs an array of newbins with lower edges x values of newbins
  TH1F *data1_rebinned = (TH1F*)data1 -> Rebin(numbins, "data1_rebinned", newBins);
  TH1F **histoarraymumu_rebinned = new TH1F*[n];
  TH1F **histoarrayemu_rebinned = new TH1F*[n];
  for (Int_t i=0; i<n; i++)
  {
    histoarraymumu_rebinned[i] = (TH1F*)histoarraymumu[i] -> Rebin(numbins, "mumu_rebinned", newBins);
    histoarrayemu_rebinned[i] = (TH1F*)histoarrayemu[i] -> Rebin(numbins, "emu_rebinned", newBins);
  }


  // N (EMU) OBSERVED
  // from Data_Nov2015/Run2015MuonsOnly/ana_datamc_data.root/EmuVetoMuonsElectronsOppSignHistos
  std::cout << endl << "N_emu_obs: ";
  Float_t N_emu_obs[numbins];
  for (Int_t i=0; i<numbins; i++)
  {
    N_emu_obs[i] = data1_rebinned -> GetBinContent(i+1);
    cout << N_emu_obs[i] << ", ";
  }


  // SCALE FACTOR FROM SINGLE CONTRIBUTIONS
  // SF_MC = N_MC -> MU+MU- / N_MC -> EMU
  TH1F **SF = new TH1F*[n];
  for (Int_t j=0; j<n; j++)
  {
    std::cout << endl << "SF_" << j << ": ";
    SF[j] = new TH1F("SF", "SF", numbins, newBins);
    SF[j] -> Divide(histoarraymumu_rebinned[j], histoarrayemu_rebinned[j]);
    for (Int_t i=0; i<numbins; i++)
    {
      std::cout << SF[j] -> GetBinContent(i+1) << ", ";
    }
  }


  // N (MU+MU-) DATA PREDICTION = N (EMU) OBSERVED * SF
  std::cout << endl << "N_mumu_data: ";
  Float_t N_mumu_data[numbins];
  for (Int_t i=0; i<numbins; i++)
  {
    N_mumu_data[i] = N_emu_obs[i] * SF[1] -> GetBinContent(i+1);
    std::cout << N_mumu_data[i] << ", ";
  }


  // N (MU+MU-) SIM. PREDICTION = N (EMU) MC * SF
  std::cout << endl << "N_mumu_sim: ";
  Float_t N_mumu_sim[numbins];
  for (Int_t i=0; i<numbins; i++)
  {
    N_mumu_sim[i] = histoarrayemu_rebinned[1] -> GetBinContent(i+1) * SF[1] -> GetBinContent(i+1);
    std::cout << N_mumu_sim[i] << ", ";
  }


  // FRACTIONS
  // Integral of all mc backgounds
  TH1F *mcsum = new TH1F("mcsum", "mcsum", numbins, newBins);
  for (Int_t i=1; i<=numbins; i++)
  {
    for (Int_t j=0; j<n; j++)
    mcsum -> AddBinContent(i, histoarrayemu_rebinned[j] -> GetBinContent(i));
  }
  // Fraction for every mc backgound
  TH1F **fraction = new TH1F*[n];
  for (Int_t j=0; j<n; j++)
  {
    fraction[j] = new TH1F("fraction", "fraction", numbins, newBins);
    fraction[j] -> Divide(histoarrayemu_rebinned[j], mcsum);
  }


  // WEIGHTED SCALE FACTORS
  TH1F **SF_weighted = new TH1F*[n];
  //std::cout << endl << "SF_weighted: ";
  for (Int_t j=0; j<n; j++)
  {
    SF_weighted[j] = new TH1F("SF_weighted", "SF_weighted", numbins, newBins);
    SF_weighted[j] -> Multiply(SF[j], fraction[j]);
    //cout << SF_weighted[j] -> GetBinContent(1) << endl;
  }

  // TOTAL SCALE FACTOR FROM ALL CONTRIBUTIONS
  // SFtot = SUM(j) { SF(j) * Fraction(j) }
  //std::cout << endl << "SF_tot: ";
  TH1F *SFtot = new TH1F("SFtot", "SFtot", numbins, newBins);
  for (j=0; j<n; j++)
  {
    SFtot -> Add(SF_weighted[j]);
    //std::cout << SFtot -> GetBinContent(1) << endl;
  }


  // APPEARANCE
  // TStyle
  gROOT -> SetStyle("Plain");
  gROOT -> ForceStyle();
  gStyle -> SetOptTitle(0);
  gStyle -> SetOptStat(0);
  gStyle -> SetTitleXOffset(1.5);
  gStyle -> SetTitleXSize(22);
  gStyle -> SetTitleYOffset(1.5);
  gStyle -> SetTitleYSize(22);
  gStyle -> SetLabelFont(43, "XY");
  gStyle -> SetLabelSize(19, "XY");
  gStyle -> SetLabelOffset(0.01, "X");
  gStyle -> SetLabelOffset(0.01, "Y");
  gStyle -> SetTitleFont(43, "XY");
  gStyle -> SetTextFont(42);
  //gStyle -> SetPaintTextFormat("5.2f");
  // Histograms appearance
  histoarrayemu_rebinned[0] -> SetLineColor(kRed);
  histoarraymumu_rebinned[0] -> SetLineColor(kTeal-1);
  SF[0] -> SetLineColor(kMagenta);
  SF[4] -> SetLineColor(kAzure+2);
  SFtot -> SetLineColor(kOrange+2);
  Int_t icolour = 0;
  Color_t colours[] = {kMagenta, kRed+1, kRed, kGreen+1, kAzure+2, kOrange-3, kViolet+1};
  for (Int_t j=0; j<n; j++)
  {
    fraction[j] -> SetLineColor(colours[icolour++]);
  }


  // -- DRAW TTBAR EVENTS
  TCanvas c1("TtbarEvents", "TtbarEvents", 0, 0, 1000, 800);
  c1.SetLogy();
  c1.SetLeftMargin(0.13);
  c1.SetBottomMargin(0.16);
  TH2D* rangesitter = new TH2D("rangesitter", "rangesetter", 100, 120, 600, 100, 50, 1e4);
  rangesitter -> GetYaxis() -> SetTitle("Reconstructed events / bin");
  rangesitter -> GetXaxis() -> SetTitle("Dilepton mass (GeV)");
  rangesitter -> Draw();
  histoarrayemu_rebinned[0] -> Draw("Same");
  histoarraymumu_rebinned[0] -> Draw("Same");
  // Legend
  leg = new TLegend(0.76,0.73,0.87,0.86);
  leg -> SetTextFont(43);
  leg -> SetTextSizePixels(20);
  leg -> SetBorderSize(0);
  leg -> SetMargin(0.5);
  leg -> SetFillColor(kGray);
  leg -> SetFillColorAlpha(1, 0.05);
  leg -> AddEntry(histoarrayemu_rebinned[0], " e^{#pm}#mu^{#mp}",  "l");
  leg -> AddEntry(histoarraymumu_rebinned[0], " #mu^{+}#mu^{-}",  "l");
  leg -> Draw();
  // Title
  TLatex *texf = new TLatex(0.45,0.94,"TTbar events");
  texf -> SetTextFont(43);
  texf -> SetTextSize(30);
  texf -> SetNDC(kTRUE);
  texf -> Draw();
  // Update
  c1.Update();
  gStyle -> SetLineScalePS(1.5);


  // -- DRAW SCALE FACTORS ttbar, WW and SF_tot
  TCanvas c2("ScaleFactors", "ScaleFactors", 0, 0, 1000, 800);
  c2.SetLeftMargin(0.13);
  c2.SetBottomMargin(0.16);
  TH2D* rangesitter2 = new TH2D("rangesitter", "rangesetter", 100, 120, 600, 100, 0.35, 1.);
  rangesitter2 -> GetYaxis() -> SetTitle("#mu#mu/e#mu scale factor");
  rangesitter2 -> GetXaxis() -> SetTitle("Dilepton mass (GeV)");
  rangesitter2 -> Draw();
  SF[4] -> Draw("Same");
  SF[0] -> Draw("Same");
  SFtot -> Draw("same");
  // Legend
  leg = new TLegend(0.19,0.74,0.3,0.87);
  leg -> SetTextFont(43);
  leg -> SetTextSizePixels(20);
  leg -> SetBorderSize(0);
  leg -> SetMargin(0.5);
  leg -> SetFillColor(kGray);
  leg -> SetFillColorAlpha(1, 0.05);
  leg -> AddEntry(SF[0], " t#bar{t}",  "l");
  leg -> AddEntry(SF[4], " WW",  "l");
  leg -> AddEntry(SFtot, " All",  "l");
  leg -> Draw();
  // Title
  TLatex *texf2 = new TLatex(0.45,0.94,"Scale factors");
  texf2 -> SetTextFont(43);
  texf2 -> SetTextSize(30);
  texf2 -> Draw();
  texf2 -> SetNDC(kTRUE);
  // Update
  c2.Update();
  gStyle -> SetLineScalePS(1.5);


  // -- DRAW FRACTIONS
  TCanvas c3("Fractions", "Fractions", 0, 0, 1000, 800);
  c3.SetLeftMargin(0.13);
  c3.SetBottomMargin(0.16);
  c3.SetLogy();
  TH2D* rangesitter3 = new TH2D("rangesitter", "rangesetter", 100, 120, 600, 100, 1e-3, 1.);
  rangesitter3 -> GetYaxis() -> SetTitle("Fraction");
  rangesitter3 -> GetXaxis() -> SetTitle("Dilepton mass (GeV)");
  rangesitter3 -> Draw();
  for (Int_t j=0; j<n; j++)
  {
    fraction[j] -> Draw("same,hist, TEXT0");
  }
  // Legend
  leg = new TLegend(0.72,0.26,0.87,0.46);
  leg -> SetTextFont(43);
  leg -> SetTextSizePixels(16);
  leg -> SetBorderSize(0);
  leg -> SetMargin(0.5);
  leg -> SetFillColor(kGray);
  leg -> SetFillColorAlpha(1, 0.05);
  leg -> AddEntry(fraction[0], " t#bar{t}",  "l");
  leg -> AddEntry(fraction[3], " W + jets",  "l");
  leg -> AddEntry(fraction[4], " WW",  "l");
  leg -> AddEntry(fraction[2], " tWt",  "l");
  leg -> AddEntry(fraction[1], " tW#bar{t}",  "l");
  leg -> AddEntry(fraction[5], " WZ",  "l");
  leg -> AddEntry(fraction[6], " ZZ",  "l");
  leg -> Draw();
  // Title
  TLatex *texf3 = new TLatex(0.45,0.94,"Fractions");
  texf3 -> SetTextFont(43);
  texf3 -> SetTextSize(30);
  texf3 -> Draw();
  texf3 -> SetNDC(kTRUE);
  // Update
  c3.Update();
  gStyle -> SetLineScalePS(2);



  // SAVING FILE FOR WRITING LATEX OUTPUT
  // Open a new ROOT file
  TFile *texout = TFile::Open("texoutput.root", "RECREATE");
  // Create storage TGraphs
  Float_t x[] = {1,2,3};
  TGraph *T_SFtot = new TGraph (SFtot);
  TGraph *T_N_emu_obs = new TGraph (numbins, x, N_emu_obs);
  TGraph *T_N_mumu_data = new TGraph (numbins, x, N_mumu_data);
  TGraph *T_N_mumu_sim = new TGraph (numbins, x, N_mumu_sim);
  texout -> cd();
  // Write TGraphs
  T_SFtot -> Write("SFtot");
  T_N_emu_obs -> Write("N_emu_obs");
  T_N_mumu_sim -> Write("N_mumu_sim");
  T_N_mumu_data -> Write("N_mumu_data");
  texout -> ls();
  delete texout;

}
