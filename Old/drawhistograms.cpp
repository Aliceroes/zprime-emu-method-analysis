{
  /**
       Opens a data rootfile, then a list of mc rootfiles all contained in the
       same directory, and draws (top pad) the histograms:
       "Our2012MuonsPlusMuonsMinusHistos/DileptonMass" from data file
       "EmuVetoMuonsElectronsOppSignHistos/DileptonMass" from every mc file
       and (bottom pad) the ratio DATA/MC
  */


  gROOT -> Reset();


  // -- OPEN DATA ROOTFILE AND TAKE "DileptonMass" HISTOGRAM
  Char_t datafilename[500] = "/afs/cern.ch/work/a/aalfonsi/Files_analisi/Data_Nov2015/Run2015MuonsOnly/ana_datamc_data.root";
  TFile *f = new TFile(datafilename);
  if (!f || f -> IsZombie())
    return;
  std::cout << ".................................................\n";
  std::cout << "Loaded data file: " << std::endl;
  std::cout << "* - " << datafilename << std::endl;
  TH1F * data1 = (TH1F*)f -> Get("EmuVetoMuonsElectronsOppSignHistos/DileptonMass");
  data1 -> SetDirectory(0);
  f -> Close();


  // -- OPEN MC ROOTFILES AND GET "DileptonMass" HISTOGRAMS
  // List rootfiles
  const char* ext = ".root";
  const char* dirbase = "/afs/cern.ch/work/a/aalfonsi/Files_analisi/mcStartupPUrw/";
  const char* filename;
  const char* rootfilename[100];
  Int_t n = 0;
  char* dir = gSystem -> ExpandPathName(dirbase);
  void* direxp = gSystem -> OpenDirectory(dir);
  while((filename = (char*)gSystem -> GetDirEntry(direxp)))
  {
    TString str;
    str = filename;
    if(str.EndsWith(ext))
      rootfilename[n++] = gSystem -> ConcatFileName(dir, filename);
  }
  // Now rootfilename[i] contains the absolute path of the i-th rootfile
  // Book TFiles / histograms
  TFile **filearray = new TFile*[n];
  TH1F **histoarray = new TH1F*[n];
  std::cout << "Loaded mc files: " << std::endl;
  for (Int_t i = 0; i < n; i++)
  {
    Char_t currentfile[500];
    sprintf(currentfile,"%s",rootfilename[i]);
    filearray[i] = new TFile(currentfile);
    if (!filearray[i] || filearray[i] -> IsZombie())
      return;
    TH1::AddDirectory(kFALSE);
    histoarray[i] = (TH1F*)filearray[i] -> Get("EmuVetoMuonsElectronsOppSignHistos/DileptonMass");
    std::cout << i << " - " << rootfilename[i] << std::endl;
    filearray[i] -> Close();
  }
  gSystem -> FreeDirectory(direxp);


  // -- NORMALIZE HISTOGRAMS
  // Calculate weight factors
  // w_norm = Lumi_data / Lumi_mc = Lumi_data / (N_mc / xsection_mc)
  Float_t Lumi_data = 2670.745 ;               // Right luminosity value;
  Float_t Lumi_mc_inv[16];                     // Lumi_mc^(-1)
  Float_t w_norm[16];
  Lumi_mc_inv[12] = 61500./24230960.;          // W + jets
  Lumi_mc_inv[15] = 15.4/998848.;              // ZZ
  Lumi_mc_inv[14] = 66.1/996920;               // WZ
  Lumi_mc_inv[13] = 118.7/995192.;             // WW
  Lumi_mc_inv[10] = 35.85/1000000.;            // tWtbar
  Lumi_mc_inv[11] = 35.85/998400.;             // tWt
  Lumi_mc_inv[9] = 815.96/19699896.;           // ttbar
  //6025.2/13344402.;                          // DY inclusive
  Lumi_mc_inv[7] = (1975.*1.006)/2901238.;     // DY 50 - 120
  Lumi_mc_inv[0] = (19.32*1.006)/100000.;      // DY 120 - 200
  Lumi_mc_inv[2] = (2.731*1.006)/100000.;      // DY 200 - 400
  Lumi_mc_inv[5] = (0.241*1.006)/100000.;      // DY 400 - 800
  Lumi_mc_inv[8] = (0.01678*1.006)/100000.;    // DY 800 - 1400
  Lumi_mc_inv[1] = (0.00139*1.006)/99600.;     // DY 1400 - 2300
  Lumi_mc_inv[3] = (0.00008948*1.006)/100000.; // DY 2300 - 3500
  Lumi_mc_inv[4] = (0.0000041*1.006)/100000.;  // DY 3500 - 4500
  Lumi_mc_inv[6] = (4.56E-7*1.006)/100000.;    // DY 4500 - 6000
  for (Int_t i=0; i<16; i++)
  {
    w_norm[i] = Lumi_data * Lumi_mc_inv[i];
    histoarray[i] -> Scale(w_norm[i]);
  }


  // -- COMBINE HISTOGRAMS
  // Merge Drell-Yan ranges
  TList *dy_list = new TList;
  for (Int_t h=0; h<9; h++)
    dy_list -> Add(histoarray[h]);
  TH1F *dy_merge = new TH1F;
  dy_merge -> Merge(dy_list);
  // Merge tWt and tWtbar
  TList *tWt_list = new TList;
  tWt_list -> Add(histoarray[10]);
  tWt_list -> Add(histoarray[11]);
  TH1F *tWt_merge = new TH1F;
  tWt_merge -> Merge(tWt_list);
  // Merge Dibosons
  TList *Di_list = new TList;
  Di_list -> Add(histoarray[13]);
  Di_list -> Add(histoarray[14]);
  Di_list -> Add(histoarray[15]);
  TH1F *Di_merge = new TH1F;
  Di_merge -> Merge(Di_list);


  // -- CREATE NEW HISTOGRAMS' ARRAY (this will speed up things later on)
  const Int_t nh = 5;
  TH1F **final_histoarray = new TH1F*[nh];
  final_histoarray[0] = histoarray[9];   // ttbar
  final_histoarray[1] = histoarray[12];  // W + jets
  final_histoarray[2] = dy_merge;        // merged Drell-Yan
  final_histoarray[3] = tWt_merge;       // merged tWt and tWtbar
  final_histoarray[4] = Di_merge;        // merged ZZ, WW and ZW


  // -- REBIN HISTOGRAMS
  data1 -> Rebin(20);
  for (Int_t i=0; i<nh; i++)
  {
    final_histoarray[i] -> Rebin(20);
  }


  // -- STACK MC HISTOGRAMS
  // Find stacking order
  Float_t weight[nh];
  // Float_t MCtotal = 0.;    * For lumi approximation
  for (Int_t v=0; v<nh; v++)
  {
    weight[v] = final_histoarray[v] -> GetSumOfWeights();
    // MCtotal = MCtotal + weight[v];   // * Calculate the total number of MC events for lumi approximation
  }
  Int_t w_index[nh];
  TMath::Sort(nh,weight,w_index);
  // Stack
  THStack hs("hs","stacked histograms");
  for (Int_t v=4; v>-1; v--)
  {
    hs.Add(final_histoarray[w_index[v]]);
  }


  /* Calculate scale factor to be used as "luminosity"
  No longer needed: now I have the right lumi factor
  Float_t datatotal = data1 -> GetSumOfWeights();
  Float_t scaleMCdata = datatotal/MCtotal;
  // if scaleMCdata replaces Lumi_data, this should return 1
  std::cout << std::endl << scaleMCdata << std::endl;
  */


  // DIVIDE DATA / MC
  TH1F *datamc_division = new TH1F("datamc_division", "", 1000, 0, 20000);
  TH1F *stacksum = (TH1F*)hs.GetStack() -> Last();
  datamc_division -> Divide(data1, stacksum);


  // APPEARANCE
  // TStyle
  gROOT -> SetStyle("Plain");
  gROOT -> ForceStyle();
  gStyle -> SetOptTitle(0);
  gStyle -> SetOptStat(0);
  gStyle -> SetTitleXOffset(4.);
  gStyle -> SetTitleXSize(20);
  gStyle -> SetTitleYOffset(1.5);
  gStyle -> SetTitleYSize(20);
  gStyle -> SetLabelFont(43, "X");
  gStyle -> SetLabelSize(16, "X");
  gStyle -> SetLabelFont(43, "Y");
  gStyle -> SetLabelSize(16, "Y");
  gStyle -> SetLabelOffset(0.02, "X");
  gStyle -> SetLabelOffset(0.01, "Y");
  gStyle -> SetTitleFont(43, "Y");
  gStyle -> SetTitleFont(43, "X");
  // Histograms appearance
  Color_t colours[] = {kAzure+10, kGreen+1, kRed, kOrange, kMagenta};
  TObjArray *histos = hs.GetStack();
  TIter Next(histos);
  TH1F *hist;
  Int_t icolour = 0;
  while ((hist =(TH1F*)Next()))
  {
    hist -> SetFillColor(colours[icolour++]);
    hist -> SetOption("HIST");
  }
  data1 -> SetLineColor(1);
  data1 -> SetMarkerStyle(20);
  data1 -> SetMarkerSize(0.5);
  datamc_division -> SetMarkerSize(0.5);
  datamc_division -> SetMarkerStyle(20);
  datamc_division -> SetLineColor(1);


  // -- DRAW HISTOGRAMS
  // To easily set the x, y range for all the histograms I am creating a new
  // empty TH2D for each pad and then printing my histograms on top of it
  TH2D* rangesitter = new TH2D("rangesitter", "rangesetter", 100, 100, 2000, 100, 1e-2, 1e4);
  TH2D* rangesitterb = new TH2D("rangesitterb", "rangesetterb", 100, 100, 2000, 100, 0., 2.);
  // Canvas & pads
  TCanvas c1("MC and data histograms", "MC and data histograms", 0, 0, 1000, 700);
  TPad *pad1 = new TPad("pad1", "Colorful pad", 0.035, 0.4, 1., 1., 0, -1, 1);
  TPad *pad2 = new TPad("pad2", "Data/MC pad", 0.035, 0., 1., 0.416, 0, -1, 1);
  pad1 -> Draw();
  pad2 -> Draw();
  // Top pad
  pad1 -> cd();
  pad1 -> SetLogy();
  pad1 -> SetBottomMargin(0.02);
  pad1 -> SetFillStyle(4000);
  rangesitter -> GetXaxis() -> SetTickLength(0);
  rangesitter -> GetXaxis() -> SetLabelOffset(999);
  rangesitter -> GetYaxis() -> SetTitle("Events / 20 GeV");
  rangesitterb -> GetXaxis() -> SetTitle("mass_{e#mu} (GeV)");
  rangesitterb -> GetYaxis() -> SetTitle("Data / MC ");
  rangesitter -> Draw();
  hs.Draw("Same,HIST");
  data1 -> Draw("Same");
  rangesitter -> Draw("sameaxis");
  // Legend
  leg = new TLegend(0.76,0.5,0.92,0.83);
  leg -> SetTextFont(43);
  leg -> SetTextSizePixels(14);
  leg -> SetBorderSize(0);
  leg -> SetFillColorAlpha(0, 0.);
  leg -> AddEntry(data1, "Data 2015",  "fl");
  leg -> AddEntry((TH1F*)histos -> At(0), "#gamma / Z #rightarrow #mu^{+}#mu^{-}",  "fl");
  leg -> AddEntry((TH1F*)histos -> At(3), "Dibosons",  "fl");
  leg -> AddEntry((TH1F*)histos -> At(2), "tWt + tW#bar{t}",  "fl");
  leg -> AddEntry((TH1F*)histos -> At(4), "t#bar{t}",  "fl");
  leg -> AddEntry((TH1F*)histos -> At(1), "W + jets",  "fl");
  leg -> Draw();
  // Bottom pad
  pad2 -> cd();
  pad2 -> SetTopMargin(0.01);
  pad2 -> SetBottomMargin(0.28);
  pad2 -> SetFillStyle(4000);
  rangesitterb -> Draw();
  TLine *line = new TLine(100,1,2000,1);
  line -> SetLineColor(kGray+1);
  line -> Draw();
  datamc_division -> Draw("Same");
  // Label adjustments: hiding one label at the superposition of the two canvas
  Float_t labelsize = datamc_division -> GetXaxis() -> GetLabelSize();
  Float_t leftmargin = pad2 -> GetLeftMargin();
  erase = new TPad("eraselabel", "eraselabel", leftmargin-labelsize, 1-labelsize, leftmargin-0.005, 1);
  erase -> Draw();
  erase -> SetFillColor(pad2 -> GetFillColor());
  erase -> SetBorderMode(0);
  c1.cd();
  pad1 -> Draw();


  // UPDATE
  c1.Update();
  gPad -> Update();
  gStyle -> SetLineScalePS(1.5);
}
